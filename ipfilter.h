/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: ipfilter.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _IPFILTER_H
#define _IPFILTER_H

unsigned char flex_ipfilter(char *input, int linenum);

#endif
