/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: cisco_ios.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _CISCO_IOS_H
#define _CISCO_IOS_H

unsigned char flex_cisco_ios(char *input, int linenum);

#endif
