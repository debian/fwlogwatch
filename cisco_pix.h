/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: cisco_pix.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _CISCO_PIX_H
#define _CISCO_PIX_H

unsigned char flex_cisco_pix(char *input, int linenum);

#endif
